package com.michal.czarnecki.hoover.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.michal.czarnecki.hoover.AppConfig;
import com.michal.czarnecki.hoover.dto.InputDataDto;
import com.michal.czarnecki.hoover.dto.OutputDataDto;
import com.michal.czarnecki.hoover.service.CleaningService;
import com.michal.czarnecki.hoover.service.InputDataValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@WebAppConfiguration
public class HooverRestControllerTest {
    private MockMvc mvc;

    @InjectMocks
    private HooverRestController hooverRestController;

    @Mock
    private CleaningService cleaningService;

    @Mock
    private InputDataValidator inputDataValidator;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders
                .standaloneSetup(hooverRestController)
                .build();
    }

    @Test
    public void shouldReturnProperHttpResponse() throws Exception {
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(5, 5))
                .coords(Arrays.asList(1, 2))
                .patch(Arrays.asList(1, 0))
                .patch(Arrays.asList(2, 2))
                .patch(Arrays.asList(2, 3))
                .instructions("NNESEESWNWW")
                .build();

        OutputDataDto outputDataDto = OutputDataDto.builder()
                .coords(Arrays.asList(1,3))
                .patchesCount(1)
                .build();

        when(cleaningService.cleanRoom(any())).thenReturn(outputDataDto);

        MvcResult mvcResult = this.mvc.perform(
                post("/hoover/cleanRoom")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(inputData)))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode result = mapper.readTree(mvcResult.getResponse().getContentAsString());
        JsonNode expectedResult = mapper.readTree(toJson(outputDataDto));

        assertEquals(expectedResult, result);
    }

    private static String toJson(final Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }

}