package com.michal.czarnecki.hoover.service;

import com.michal.czarnecki.hoover.AppConfig;
import com.michal.czarnecki.hoover.dto.InputDataDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class InputDataValidatorTest {
    @Autowired
    private InputDataValidator inputDataValidator;

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenInputPositionIsOutOfRoom() {
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(5,5))
                .coords(Arrays.asList(100,0))
                .build();
        inputDataValidator.validate(inputData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenCoordinatesAreNegative() {
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(5,5))
                .coords(Arrays.asList(-4,0))
                .build();
        inputDataValidator.validate(inputData);
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenRoomSizeIsNegative() {
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(-5,5))
                .coords(Arrays.asList(4,0))
                .build();
        inputDataValidator.validate(inputData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenRoomSizeIsZero() {
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(0,5))
                .coords(Arrays.asList(0,0))
                .build();
        inputDataValidator.validate(inputData);
    }

}