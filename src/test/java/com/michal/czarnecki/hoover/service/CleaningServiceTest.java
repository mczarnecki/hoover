package com.michal.czarnecki.hoover.service;

import com.michal.czarnecki.hoover.AppConfig;
import com.michal.czarnecki.hoover.entity.Coordinate;
import com.michal.czarnecki.hoover.dto.InputDataDto;
import com.michal.czarnecki.hoover.dto.OutputDataDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class CleaningServiceTest {
    @Autowired
    private CleaningService cleaningService;

    @Test
    public void shouldComeBackToTheSamePosition() {
        assertPosition("", new Coordinate(0, 0)); //not moving anywhere
        assertPosition("NS", new Coordinate(0, 0));
        assertPosition("NESW", new Coordinate(0, 0));
        assertPosition("NNEESSWW", new Coordinate(0, 0));
    }

    @Test
    public void shouldNotExceedWall() {
        assertPosition("NNNNNNN", new Coordinate(0, 4));
        assertPosition("W", new Coordinate(0, 0));
        assertPosition("EEEEE", new Coordinate(4, 0));
        assertPosition("S", new Coordinate(0, 0));
        assertPosition("NENENENENENENENE", new Coordinate(4, 4));
    }

    @Test
    public void shouldCleanPatches() {
        assertCleanedPatchesCount("", 1); //not moving anywhere
        assertCleanedPatchesCount("NNEEEE", 2);
    }

    @Test
    public void shouldNotCleanPatchesTwice() {
        assertCleanedPatchesCount("NENENENEWSWSWSW", 5);
    }

    @Test
    public void shouldNotCleanPatchesOutsideTheRoom() {
        assertCleanedPatchesCount("NENENENENENENENENE", 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForInvalidInstructions() {
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(5, 5))
                .coords(Arrays.asList(0, 0))
                .instructions("QWERTY")
                .build();
        cleaningService.cleanRoom(inputData);
    }

    private void assertPosition(String instructions, Coordinate expectedPosition) {
        //given
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(5, 5))
                .coords(Arrays.asList(0, 0))
                .instructions(instructions)
                .build();

        //when
        OutputDataDto out = cleaningService.cleanRoom(inputData);

        //then
        Coordinate finalPosition = getFinalPosition(out);
        assertEquals(expectedPosition, finalPosition);
    }

    private void assertCleanedPatchesCount(String instructions, int expectedCleanedPatchesCount) {
        //given
        InputDataDto inputData = InputDataDto.builder()
                .roomSize(Arrays.asList(5, 5))
                .coords(Arrays.asList(0, 0))
                .patch(Arrays.asList(0, 0))
                .patch(Arrays.asList(1, 1))
                .patch(Arrays.asList(2, 2))
                .patch(Arrays.asList(3, 3))
                .patch(Arrays.asList(4, 4))
                .patch(Arrays.asList(4, 4)) //patch at the same place
                .patch(Arrays.asList(6, 6)) //patch outside the room
                .instructions(instructions)
                .build();

        //when
        OutputDataDto out = cleaningService.cleanRoom(inputData);

        //then
        assertEquals(expectedCleanedPatchesCount, out.getPatchesCount());
    }

    private Coordinate getFinalPosition(final OutputDataDto out) {
        return new Coordinate(out.getCoords().get(0), out.getCoords().get(1));
    }

}