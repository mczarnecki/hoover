package com.michal.czarnecki.hoover.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "OUTPUT_DATA")
/**
 * Output data for JPA
 */
public class OutputDataEntity {
    @Id
    @GeneratedValue
    @Column(name="output_data_id")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private Coordinate coords;

    @Basic
    private int patches;
}
