package com.michal.czarnecki.hoover.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "HOOVERING")
/**
 * represents root JPA object for single hoovering
 */
public class HooveringEntity {
    @Id
    @GeneratedValue
    @Column(name="HOVERING_ID")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private InputDataEntity inputData;

    @OneToOne(cascade = CascadeType.ALL)
    private OutputDataEntity outputData;

}
