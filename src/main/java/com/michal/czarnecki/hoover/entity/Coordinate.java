package com.michal.czarnecki.hoover.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "COORDINATES")
/**
 * Represents pair (x,y)
 */
public class Coordinate {
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Id
    @GeneratedValue
    @Column(name = "COORDINATES_ID", nullable = false)
    private Long id;

    @Basic
    private int x;

    @Basic
    private int y;

    @Override
    public String toString() {
        return "[" + x + "," + y + "]";
    }
}
