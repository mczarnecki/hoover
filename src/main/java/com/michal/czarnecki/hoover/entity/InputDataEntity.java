package com.michal.czarnecki.hoover.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "INPUT_DATA")
/**
 * Input data for JPA
 */
public class InputDataEntity {
    @Id
    @GeneratedValue
    @Column(name="INPUT_DATA_ID")
    private Long id;

    @Singular
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Coordinate> patches;

    @OneToOne(cascade = CascadeType.ALL)
    private Coordinate roomSize;

    @OneToOne(cascade = CascadeType.ALL)
    private Coordinate coords;

    @Basic
    private String instructions;
}
