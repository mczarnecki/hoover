package com.michal.czarnecki.hoover.enums;

import com.michal.czarnecki.hoover.dto.InputDataDto;

public enum Direction {
    NORTH ("N"),
    SOUTH ("S"),
    EAST ("E"),
    WEST ("W");

    private String strValue;

    Direction(String strValue) {
        this.strValue = strValue;
    }

    /**
     * Validates text instruction from {@link InputDataDto}
     * @param strValue
     * @return
     */
    public static Direction fromString(String strValue) {
        for (Direction b : Direction.values()) {
            if (b.strValue.equalsIgnoreCase(strValue)) {
                return b;
            }
        }
        throw new IllegalArgumentException("No Direction for " + strValue + " found");
    }

}
