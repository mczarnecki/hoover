package com.michal.czarnecki.hoover.rest;

import com.michal.czarnecki.hoover.dto.InputDataDto;
import com.michal.czarnecki.hoover.dto.OutputDataDto;
import com.michal.czarnecki.hoover.service.CleaningService;
import com.michal.czarnecki.hoover.service.InputDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exposes hoover service via REST
 */
@RestController
public class HooverRestController {
    private CleaningService cleaningService;
    private InputDataValidator inputDataValidator;

    @Autowired
    public HooverRestController(CleaningService cleaningService, InputDataValidator inputDataValidator) {
        this.cleaningService = cleaningService;
        this.inputDataValidator = inputDataValidator;

    }

    @RequestMapping(method = RequestMethod.POST, value = "/hoover/cleanRoom")
    public OutputDataDto cleanRoom(@RequestBody InputDataDto inputDataDto) {
        inputDataValidator.validate(inputDataDto);
        return cleaningService.cleanRoom(inputDataDto);
    }

}
