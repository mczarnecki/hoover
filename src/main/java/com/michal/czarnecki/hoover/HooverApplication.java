package com.michal.czarnecki.hoover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class HooverApplication {
    public static void main(String[] args) {
        SpringApplication.run(HooverApplication.class, args);
    }
}
