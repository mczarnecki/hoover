package com.michal.czarnecki.hoover.service;

import com.google.common.base.Preconditions;
import com.michal.czarnecki.hoover.dto.InputDataDto;
import org.springframework.stereotype.Service;

/**
 * Validate data received in HTTP request
 */
@Service
public class InputDataValidator {
    private static final String INCORRECT_ROOM_SIZE = "Incorrect room size";
    private static final String INCORRECT_COORDINATES = "Incorret coordinates";
    private static final String INPUT_POSITION_OUT_OF_ROOM = "Input hoover position must be inside room";

    public void validate(final InputDataDto inputDataDto) {
        Preconditions.checkArgument(inputDataDto.getCoords().size() == 2,INCORRECT_COORDINATES);
        Preconditions.checkArgument(inputDataDto.getCoords().get(0) >=0, INCORRECT_COORDINATES);
        Preconditions.checkArgument(inputDataDto.getCoords().get(1) >=0, INCORRECT_COORDINATES);

        Preconditions.checkArgument(inputDataDto.getRoomSize().size() == 2, INCORRECT_ROOM_SIZE);
        Preconditions.checkArgument(inputDataDto.getRoomSize().get(0) >0, INCORRECT_ROOM_SIZE);
        Preconditions.checkArgument(inputDataDto.getRoomSize().get(1) >0, INCORRECT_ROOM_SIZE);

        Preconditions.checkArgument(isInputPositionInsideRoom(inputDataDto),INPUT_POSITION_OUT_OF_ROOM);

    }

    private boolean isInputPositionInsideRoom(final InputDataDto inputData) {
        return (inputData.getCoords().get(0) < inputData.getRoomSize().get(0) &&
                inputData.getCoords().get(1) < inputData.getRoomSize().get(1));
    }

}
