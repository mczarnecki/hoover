package com.michal.czarnecki.hoover.service;

import com.michal.czarnecki.hoover.dto.InputDataDto;
import com.michal.czarnecki.hoover.dto.OutputDataDto;
import com.michal.czarnecki.hoover.entity.HooveringEntity;
import com.michal.czarnecki.hoover.entity.InputDataEntity;
import com.michal.czarnecki.hoover.enums.Direction;
import com.michal.czarnecki.hoover.jpa.HooveringRepository;
import com.michal.czarnecki.hoover.move.MovingEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
/**
 * Main service responsible for single hoovering
 * Runs hoover, gathers output data
 * Persists output data in DB
  */
public class CleaningService {
    private MovingStrategyFactory movingStrategyFactory;
    private DtoToOutputEntityConverter dtoToOutputEntityConverter;
    private DtoToInputDataEntityConverter dtoToInputDataEntityConverter;
    private HooveringRepository hooveringRepository;


    @Autowired
    public CleaningService(MovingStrategyFactory movingStrategyFactory,
                           DtoToInputDataEntityConverter dtoToInputDataEntityConverter,
                           HooveringRepository hooveringRepository,
                           DtoToOutputEntityConverter dtoToOutputEntityConverter) {
        this.movingStrategyFactory = movingStrategyFactory;
        this.dtoToInputDataEntityConverter = dtoToInputDataEntityConverter;
        this.hooveringRepository = hooveringRepository;
        this.dtoToOutputEntityConverter = dtoToOutputEntityConverter;
    }

    public OutputDataDto cleanRoom(final InputDataDto inputDataDto) {
        //Convert input data
        InputDataEntity inputDto = dtoToInputDataEntityConverter.convert(inputDataDto);

        List<Direction> instructions = new ArrayList<>();
        if (!inputDataDto.getInstructions().isEmpty()) {
            instructions = Arrays.stream(inputDataDto.getInstructions().split("(?!^)"))
                    .map(Direction::fromString)
                    .collect(Collectors.toList());
        }

        //Initialize moving engine
        MovingEngine movingEngine = new MovingEngine(inputDto);

        //move hoover
        for (Direction instruction : instructions) {
            movingEngine.move(movingStrategyFactory.getMovingStrategy(instruction));
        }

        //build output data
        OutputDataDto outputDto = OutputDataDto.builder()
                .coord(movingEngine.getCurrentPosition().getX())
                .coord(movingEngine.getCurrentPosition().getY())
                .patchesCount(movingEngine.getCleanedPatchesCount())
                .build();

        //persist hoovering
        hooveringRepository.save(HooveringEntity.builder()
                .inputData(inputDto)
                .outputData(dtoToOutputEntityConverter.convert(outputDto))
                .build());

        return outputDto;
    }

}
