package com.michal.czarnecki.hoover.service;

import com.michal.czarnecki.hoover.dto.OutputDataDto;
import com.michal.czarnecki.hoover.entity.Coordinate;
import com.michal.czarnecki.hoover.entity.OutputDataEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;


@Service
public class DtoToOutputEntityConverter implements Converter<OutputDataDto, OutputDataEntity> {
    @Override
    public OutputDataEntity convert(OutputDataDto outputDataDto) {
        return OutputDataEntity.builder()
                .coords(new Coordinate(outputDataDto.getCoords().get(0),outputDataDto.getCoords().get(1)))
                .patches(outputDataDto.getPatchesCount())
                .build();
    }
}
