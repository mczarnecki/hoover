package com.michal.czarnecki.hoover.service;


import com.michal.czarnecki.hoover.enums.Direction;
import com.michal.czarnecki.hoover.move.strategy.MovingStrategy;
import com.michal.czarnecki.hoover.move.strategy.MovingEastStrategy;
import com.michal.czarnecki.hoover.move.strategy.MovingNorthStrategy;
import com.michal.czarnecki.hoover.move.strategy.MovingSouthStrategy;
import com.michal.czarnecki.hoover.move.strategy.MovingWestStrategy;
import org.springframework.stereotype.Service;

@Service
public class MovingStrategyFactory {
    public MovingStrategy getMovingStrategy(Direction instruction) {
        MovingStrategy movingStrategy;
        switch (instruction) {
            case NORTH:
                movingStrategy = new MovingNorthStrategy();
                break;
            case SOUTH:
                movingStrategy = new MovingSouthStrategy();
                break;
            case EAST:
                movingStrategy = new MovingEastStrategy();
                break;
            case WEST:
                movingStrategy = new MovingWestStrategy();
                break;
            default:
                throw new IllegalArgumentException("Unknown instruction! " + instruction);
        }

        return movingStrategy;
    }

}
