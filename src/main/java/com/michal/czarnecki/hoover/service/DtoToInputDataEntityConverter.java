package com.michal.czarnecki.hoover.service;

import com.michal.czarnecki.hoover.dto.InputDataDto;
import com.michal.czarnecki.hoover.entity.Coordinate;
import com.michal.czarnecki.hoover.entity.InputDataEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class DtoToInputDataEntityConverter implements Converter<InputDataDto, InputDataEntity>{

    @Override
    public InputDataEntity convert(InputDataDto inputDataDto) {
        return InputDataEntity.builder()
                .coords(new Coordinate(inputDataDto.getCoords().get(0),inputDataDto.getCoords().get(1)))
                .roomSize(new Coordinate(inputDataDto.getRoomSize().get(0),inputDataDto.getRoomSize().get(1)))
                .patches(inputDataDto.getPatches().stream()
                        .map(p -> new Coordinate(p.get(0), p.get(1))).collect(Collectors.toSet()))
                .instructions(inputDataDto.getInstructions())
                .build();
    }
}
