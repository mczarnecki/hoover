package com.michal.czarnecki.hoover.dto;

import lombok.*;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
/**
 * Output DTO that is in HTTP response
 */
public class OutputDataDto {
    @Singular
    private List<Integer> coords;
    private int patchesCount;
}
