package com.michal.czarnecki.hoover.dto;


import lombok.*;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
/**
 * Input DTO received in HTTP request body
 */
public class InputDataDto {
    @Singular
    private List<List<Integer>> patches;
    private List<Integer> roomSize;
    private List<Integer> coords;
    private String instructions;
}
