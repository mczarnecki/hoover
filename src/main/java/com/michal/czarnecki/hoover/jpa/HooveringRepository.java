package com.michal.czarnecki.hoover.jpa;

import com.michal.czarnecki.hoover.entity.HooveringEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public interface HooveringRepository extends CrudRepository<HooveringEntity, String> {
}
