package com.michal.czarnecki.hoover.move.strategy;


import com.michal.czarnecki.hoover.entity.Coordinate;
import com.michal.czarnecki.hoover.enums.Direction;

public interface MovingStrategy {

    Coordinate move(Coordinate currentPosition);
    boolean isTouchingWall(Coordinate currentPosition, Coordinate roomSize);
    Direction getDirection();

}
