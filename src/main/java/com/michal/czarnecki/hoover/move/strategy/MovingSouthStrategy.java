package com.michal.czarnecki.hoover.move.strategy;


import com.michal.czarnecki.hoover.entity.Coordinate;
import com.michal.czarnecki.hoover.enums.Direction;

public class MovingSouthStrategy implements MovingStrategy {
    @Override
    public Coordinate move(final Coordinate currentPosition) {
        return new Coordinate(currentPosition.getX(), currentPosition.getY() - 1);

    }

    @Override
    public boolean isTouchingWall(final Coordinate currentPosition, final Coordinate roomSize) {
        return currentPosition.getY() == 0;
    }

    @Override
    public Direction getDirection() {
        return Direction.SOUTH;
    }
}
