package com.michal.czarnecki.hoover.move.strategy;

import com.michal.czarnecki.hoover.entity.Coordinate;
import com.michal.czarnecki.hoover.enums.Direction;

public class MovingEastStrategy implements MovingStrategy {
    @Override
    public Coordinate move(final Coordinate currentPosition) {
        return new Coordinate(currentPosition.getX() + 1, currentPosition.getY());
    }

    @Override
    public boolean isTouchingWall(final Coordinate currentPosition, final Coordinate roomSize) {
        return currentPosition.getX() == roomSize.getX() - 1;
    }

    @Override
    public Direction getDirection() {
        return Direction.EAST;
    }

}
