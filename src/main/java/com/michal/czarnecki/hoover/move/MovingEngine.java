package com.michal.czarnecki.hoover.move;


import com.michal.czarnecki.hoover.entity.Coordinate;
import com.michal.czarnecki.hoover.entity.InputDataEntity;
import com.michal.czarnecki.hoover.move.strategy.MovingStrategy;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Moves hoover to another position according to {@link MovingStrategy}
 * Counts cleaned patches
 * In case of initial position in on dirt patch, it is consider as cleaned without move
 */
public class MovingEngine {
    private final static Logger LOGGER = LoggerFactory.getLogger(MovingEngine.class);
    private final InputDataEntity inputDataEntity;
    private Set<Coordinate> patches;

    @Getter
    private int cleanedPatchesCount;

    @Getter
    private Coordinate currentPosition;

    public MovingEngine(final InputDataEntity inputDataEntity) {
        this.inputDataEntity = inputDataEntity;
        this.currentPosition = inputDataEntity.getCoords();
        this.patches = new HashSet<>(inputDataEntity.getPatches());


        LOGGER.info("Starting Hoover engine. Current position: {}", currentPosition);
        cleanIfNeeded(currentPosition);
    }

    public void move(final MovingStrategy movingStrategy) {
        Coordinate newPosition = currentPosition;
        if (!movingStrategy.isTouchingWall(currentPosition, inputDataEntity.getRoomSize())) {
            newPosition = movingStrategy.move(currentPosition);
            LOGGER.info("Moving {} from {} -> {}", movingStrategy.getDirection(), currentPosition, newPosition);
        } else {
            LOGGER.info("Cannot move {}. Wall found. Staying at {}", movingStrategy.getDirection(), newPosition);
        }
        cleanIfNeeded(newPosition);

        currentPosition = newPosition;
    }

    private void cleanIfNeeded(Coordinate position) {
        if (this.patches.contains(position)) {
            this.cleanedPatchesCount++;
            this.patches.remove(position);
            LOGGER.info("Cleaning patch at {}. Total cleaned patches: {}", position, cleanedPatchesCount);
        }
    }

}
