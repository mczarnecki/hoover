package com.michal.czarnecki.hoover;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.michal.czarnecki.hoover"})
public class AppConfig {
}
