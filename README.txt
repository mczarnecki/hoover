Requirements
----------------------------
1. JRE 8
2. JAVA_HOME environment set


Running the application
----------------------------
1. Build application and run tests:
cd hoover
./mvnw clean package

2. Run application:
./mvnw spring-boot:run


Executing HTTP requests
----------------------------
1. Execute POST request to http://localhost:8080/hoover/cleanRoom 
curl -H "Content-Type: application/json" -X POST -d '{"patches" :[[1,0],[2,2],[2,3]],"roomSize":[5,5],"coords":[1,2],"instructions":"NNESEESWNWW"}' http://localhost:8080/hoover/cleanRoom


Accessing Database
----------------------------
1. Navigate to http://localhost:8080/h2/
2. set JDBC URL: jdbc:h2:~/hoover
3. Click Connect button